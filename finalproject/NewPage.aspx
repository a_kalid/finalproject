﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="NewPage.aspx.cs" Inherits="finalproject.NewPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<asp:SqlDataSource runat="server" id="insert_page"
         ConnectionString="<%$ ConnectionStrings:final_sql_con %>">
    </asp:SqlDataSource>

    <h3>New Page</h3>

    <div class="inputrow">
        <label>Page Title:</label>
        <asp:textbox id="page_title" runat="server">
        </asp:textbox>
    </div>

    <div class="inputrow">
        <label>Page Content:</label>
        <asp:textbox id="page_content" runat="server">
        </asp:textbox>
    </div>

    

    <asp:Button Text="Add Page" runat="server" OnClick="AddPage"/>

    <div class="querybox" id="debug" runat="server">

    </div>

</asp:Content>
