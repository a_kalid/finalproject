﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ManagePages.aspx.cs" Inherits="finalproject.ManagePages" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
      
    <asp:SqlDataSource runat="server"
        id="pages_select"
       
        ConnectionString="<%$ ConnectionStrings:final_sql_con %>">
    </asp:SqlDataSource>
    <div id="pages_query" class="querybox" runat="server">

    </div>
    <asp:DataGrid id="pages_list"
        runat="server" >
    </asp:DataGrid>

    <div id="debug" runat="server"></div>




</asp:Content>