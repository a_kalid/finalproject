﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Page.aspx.cs" Inherits="finalproject.Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    
    <h3 id="page_title" runat="server"></h3>
    <a href="EditPage.aspx?pageid=<%Response.Write(this.pageid); %>">edit</a>
    <p id="page_content" runat="server"></p>
   
   <asp:SqlDataSource runat="server"
        id="page_select"
         ConnectionString="<%$ ConnectionStrings:final_sql_con %>">
    </asp:SqlDataSource>
    
   
   <asp:SqlDataSource runat="server"
        id="del_page"
         ConnectionString="<%$ ConnectionStrings:final_sql_con %>">
    </asp:SqlDataSource>
    
    
    <div id="del_debug" runat="server"></div>
    <div id="page_query" runat="server" class="querybox">
    
    </div>
   <ASP:Button Text="Del Page" runat="server" OnClick="DelPage"/>

    

</asp:Content>
    