﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumberGuesser
{
    class Program
    {
        //void means it doesnot return anything
        static void Main(string[] args)
        {
            
            //string name = "Azeb Kalid";
            //int age = 35;
            //start here for what the function does. writeLine is a function//
            //Console.WriteLine(name+ "is " +age);
            //set app vars
            GetAppInfo(); // run GetAppInfo function to get user info
            GreetUser(); //ask users name and greet
            while (true)
            {
                //set correct number
                //int correctNumber = 7;
                //create a new Random object
                Random random = new Random();
                //init correct number
                int correctNumber = random.Next(1, 10);

                int guess = 0;
                //ask user for number
                Console.WriteLine("Guess anumber between 1 and 10");
                while (guess != correctNumber)
                {
                    //get user input
                    string input = Console.ReadLine();
                    //make sure its a number
                    if (!int.TryParse(input, out guess))
                    {
                        //print error message
                        PrintColorMessage(ConsoleColor.Red, "Please use an actual number");
                        //keep going
                        continue;
                    }
                    //converts input into integer!
                    guess = Int32.Parse(input);
                    //match guess to correct number
                    if (guess != correctNumber)
                    {
                        //print error message
                        PrintColorMessage(ConsoleColor.Red, "Wrong number, please try again");
                    }
                }

                //output success message
                //change text color
                PrintColorMessage(ConsoleColor.Yellow, "Correct, you guessed it!");
                //as to play again
                Console.WriteLine("Play again? [Y or N]");

                //get answer and convert to uppercase
                string answer = Console.ReadLine().ToUpper();
                if (answer == "Y")
                {
                    continue;
                }
                else if (answer == "N")
                {
                    return;
                }
            }
        }

            //get and display app info
            static void GetAppInfo()
            {
                string appName = "Number Guesser";
                string appVersion = "1.0.0";
                string appAuthor = "Azeb Kalid";
                // change text color
                Console.ForegroundColor = ConsoleColor.Green;

                Console.WriteLine("{0}: version {1} by {2}", appName, appVersion, appAuthor);
                Console.ResetColor();
            }
            //ask user name and greet
            static void GreetUser()
            {


                Console.WriteLine("What is your name?");
                string inputName = Console.ReadLine();
                Console.WriteLine("Hello {0}, let's play a game...", inputName);
            }
            //print color message
            static void PrintColorMessage(ConsoleColor color, string message)
            {

            Console.ForegroundColor = color;
                Console.WriteLine(message);
                //reset text color
                Console.ResetColor();

            }
        
    }
}
